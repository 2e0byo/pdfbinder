#!/bin/bash
# very simple program to call pdfbook on input after splitting
set +x
set -e
__script_version="1.0"

function usage ()
{
    echo "Usage :  $0 [options] [--]

    Options:
    -h|help       Display this message
    -v|version    Display script version
    -i|input      Input file (required)
    -o|output     Output file (required)
    -n|signatures Number of Signatures
    -p|pages      Pages in Signatures

pages and signatures are mutually exclusive"


}    # ----------  end of function usage  ----------


#-----------------------------------------------------------------------
#  Handle command line arguments
#-----------------------------------------------------------------------
while getopts ":hvi:o:n:p:" opt
do
    case $opt in

	h|help     )  usage; exit 0   ;;

	v|version  )  echo "$0 -- version $__script_version"; exit 0   ;;

	i|input    )  input=$OPTARG; echo $input   ;;

	o|output ) output=$OPTARG ;;

	n|signatures )  sigs=$OPTARG ;;

	p|pages  ) pages=$OPTARG ;;

	* )  echo -e "\n  Option does not exist : $OPTARG\n"
	     usage; exit 1   ;;

    esac    # --- end of case ---
done
shift $(($OPTIND-1))

if [ ! -z ${sigs+x} ] && [ ! -z ${pages+x} ]; then
    echo "signatures and pages are mutually exclusive"
    exit 1
fi

if [  -z ${input+x} ]; then
    echo "input required"
    exit 1
fi

if [  -z ${output+x} ]; then
    echo "output required"
    exit 1
fi

# split pdf

if [  -z ${pages+x} ]; then
    echo "unimplemented, please code"
    exit 2
fi

pdfpages=$(pdfinfo $input | grep "Pages:" | awk '{print $2;}')
tdir=pdfbind_tmp
mkdir $tdir
pdftk $input  burst output 'pdfbind_tmp/p%04d.pdf'

i=1
s=1
sigcount=1
echo "Processing signature $s..."
while [ $i -lt $pdfpages ] ; do	# zip signatures together
    sig=$tdir/$(printf 'sig%04d.pdf' $s)
    if [ $sigcount = 1 ] ; then
	cp $tdir/$(printf 'p%04d.pdf' $i) $sig # first page of new signature
    fi
    ((i++))
    f1=$tdir/$(printf 'p%04d.pdf' $i)
    pdftk $sig $f1 cat output $sig.tmp
    mv $sig.tmp $sig
    ((sigcount++))
    if [ $sigcount = $pages ] ; then
	pdfbook $sig -o $sig -q
	sigcount=1
	((s++))			# new signature
	((i++))
	echo "Processing signature $s..."
    fi
done

for i in $(find $tdir -regex '.*p[0-9]*\.pdf') ; do
    rm $i
done



